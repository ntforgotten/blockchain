pragma solidity 0.5.7;

contract NotForgotten {

    address public nfAdmin;
    string public CONTACTINFO1 = "NotForgotten Digital Preservation Trust LLC, a Delaware USA company";
    string public CONTACTINFO2 = "NotForgotten Depository LLC, a Delaware USA company";
    string public CONTACTINFO3 = "A designated OCLC Library symbol IEFDP";
    string public CONTACTINFO4 = "support@not-forgotten.com";
    
    struct User{
        address userAddress;
        string fName;
        string lName;
        uint256 birthYear;
        uint256 expiryDate;
        string videoURL;
        string worldComURL;
        string gedComId;
        uint256 index;
    }

    mapping (address => User) public users;

    User[] public userArray;

    modifier onlyOwner(){
        require(nfAdmin == msg.sender, "Not the contract Owner");
        _;
    }

    event LogAddedUserData(address indexed userAddress, string indexed gedComId);
    event LogRemovedUserData(address indexed userAddress, string indexed gedComId);
    event LogEditedUserData(address indexed userAddress, string indexed gedComId, string indexed newWorldComURL);

    constructor()
        public
    {
        nfAdmin = msg.sender;
    }
    
    function addUserData(
        string memory _GEDuid, string memory _worldComURL,
        string memory _videoURL, string memory _fName,
        string memory _lName, uint256 _birthyear,
        uint256 _expDate, address _userAddress
        )
        public
        onlyOwner
    {
        require(bytes(_GEDuid).length > 0, "Empty Variable: GEDuid");
        require(bytes(_worldComURL).length > 0, "Empty Variable: worldComURL");
        require(bytes(_videoURL).length > 0, "Empty Variable: videoURL");
        require(bytes(_fName).length > 0, "Empty Variable: first name");
        require(bytes(_lName).length > 0, "Empty Variable: last name");
        require(_birthyear != 0, "Empty Variable: birth year");
        require(_expDate != 0, "Empty Variable: expiry date");
        require(_userAddress != address(0), "Empty Variable: user eth address");
        require(_expDate > now, "Date is not in the future");

        User storage user = users[_userAddress];
        user.userAddress = _userAddress;
        user.fName = _fName;
        user.lName = _lName;
        user.birthYear = _birthyear;
        user.videoURL = _videoURL;
        user.worldComURL = _worldComURL;
        user.gedComId = _GEDuid;
        user.expiryDate = _expDate;
        user.index = userArray.length;
        userArray.push(user);
        emit LogAddedUserData(_userAddress, _GEDuid);
    }

    function editUserData(address userAddress, string memory newGEDuid, string memory newWorldComURL)
        public
        onlyOwner
    {
        require(userAddress != address(0), "Empty Variable: userAddress");
        require(bytes(newGEDuid).length > 0, "Empty Variable: newGEDuid");
        require(bytes(newWorldComURL).length > 0, "Empty Variable: newWorldComURL");
        User storage user = users[userAddress];
        require(user.userAddress == userAddress, "User address is not the same as the address entered");
        user.gedComId = newGEDuid;
        user.worldComURL = newWorldComURL;
        userArray[user.index].gedComId = newGEDuid;
        userArray[user.index].worldComURL = newWorldComURL;
        emit LogEditedUserData(userAddress, newGEDuid, newWorldComURL);
    }

    function removeUserData(address userAddress)
        public
    {
        address addressToRemove;

        if(userAddress != address(0) && msg.sender == nfAdmin) {
            addressToRemove = userAddress;
        } else {
            addressToRemove = msg.sender;
        }
        
        User storage user = users[addressToRemove];
        string memory tempGEDID = user.gedComId;
        user.userAddress = address(0);
        user.fName = "";
        user.lName = "";
        user.worldComURL = "";
        user.videoURL = "";
        user.gedComId = "";
        user.expiryDate = 0;
        user.birthYear = 0;
        userArray[user.index].userAddress = address(0);
        userArray[user.index].fName = "";
        userArray[user.index].lName = "";
        userArray[user.index].videoURL = "";
        userArray[user.index].worldComURL = "";
        userArray[user.index].gedComId = "";
        userArray[user.index].expiryDate = 0;
        userArray[user.index].index = 0;
        userArray[user.index].birthYear = 0;
        user.index = 0;

        emit LogRemovedUserData(addressToRemove, tempGEDID);
    }

}
